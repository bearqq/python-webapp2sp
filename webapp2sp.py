"""
Usage:
import webapp2sp as webapp2
webapp2.server(application)
"""

outofgae=False
try:
	from webapp2 import *
except:
	import sys
	sys.path.insert(0, 'libs.zip') 
	#sys.path.append(r'/home/ubuntu/workspace/bin/webapp2-2.5.2')
	#sys.path.append(r'/home/ubuntu/workspace/bin/WebOb-1.4')
	#sys.path.append(r'/home/ubuntu/workspace/bin/Paste-1.7.5.1')
	outofgae=True
	from webapp2 import *

def server(app,ip='0.0.0.0',port=8080):
	if outofgae:
		from paste import httpserver
		import os
		httpserver.serve(app,host=os.getenv('IP',ip),port=os.getenv('PORT',port))